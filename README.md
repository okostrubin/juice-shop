# Ecosystem SRE interview assignment

## Requirements

- Docker
- Kubernetes
- Minikube

## Minikube configuration

```
minikube addons enable ingress
minikube tunnel
```

## Deployment of stack

```
deploy.sh
```

## Testing examples:

```
https://localhost/
```

## Removing stack

```
remove.sh
```

## Technologies

- Docker
- Kubernetes
- Prometheus
- Grafana

## Things to improve

- DNS
- Let's Encrypt