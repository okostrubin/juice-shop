#!/bin/sh

set -e

helm uninstall juice-shop --namespace juice-shop
kubectl delete namespace juice-shop

helm uninstall monitoring --namespace monitoring
kubectl delete namespace monitoring
kubectl delete crd alertmanagerconfigs.monitoring.coreos.com
kubectl delete crd alertmanagers.monitoring.coreos.com
kubectl delete crd podmonitors.monitoring.coreos.com
kubectl delete crd probes.monitoring.coreos.com
kubectl delete crd prometheuses.monitoring.coreos.com
kubectl delete crd prometheusrules.monitoring.coreos.com
kubectl delete crd servicemonitors.monitoring.coreos.com
kubectl delete crd thanosrulers.monitoring.coreos.com

helm uninstall nginx --namespace nginx
kubectl delete namespace nginx
kubectl delete -f crds/