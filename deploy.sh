#!/bin/sh

set -e

helm repo add securecodebox https://charts.securecodebox.io
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo add nginx-stable https://helm.nginx.com/stable
helm repo update

kubectl create namespace juice-shop
helm install juice-shop securecodebox/juice-shop --namespace juice-shop

kubectl create namespace monitoring
helm install -f monitoring.yaml monitoring prometheus-community/kube-prometheus-stack --namespace monitoring

helm install nginx nginx-stable/nginx-ingress